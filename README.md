# DESIGNING ONITAMA ADVERSARIAL GAME SEARCH

CSCM603130 Sistem Cerdas @ Faculty of Computer Science Universitas Indonesia,
Term 2 2018/2019

* * *
### Authors
* **Reynaldo Wijata Hendry** - *1706028625*
* **Annida Safira Arief** - *1706040050*
* **Lulu Ilmaknun Qurotaini** - *1706979341*
* **M. Achir Suci Ramadhan** - *1706979354*

* * *
Onitama merupakan sebuah boardgame yang berasal dari Jepang dengan papan permainan berukuran 5x5.
Dengan menggunakan Adversarial Game Search diharapkan sebuah *Intelligent Agent* yang dapat mengetahui langkah optimal yang seharusnya diambil yang dapat membawanya menuju kemenangan (yang mungkin juga merupakan strategi pemain lainnya).

* * *

## The Algorithms
Proyek ini akan dikerjakan menggunakan Adversarial Game Search. Adversarial Game Search merupakan salah satu teknik Artificial Intelligence yang biasa digunakan dalam menyelesaikan masalah yang mengandung agen yang saling kompetitif. 
Dalam kasus ini, seorang pemain tidak mengetahui pasti apa strategi pemain lainnya. Namun, seorang pemain dapat mengetahui langkah optimal yang seharusnya diambil yang dapat membawanya menuju kemenangan (yang mungkin juga merupakan 
strategi pemain lainnya).

Algoritma utama yang akan diimplementasikan dalam proyek ini adalah Algoritma Minimax. Algoritma Minimax adalah sebuah algoritma yang biasa digunakan dalam pengambilan keputusan untuk suatu Competitive Game Theory dengan asumsi kedua 
pemain bermain secara optimal. Secara sederhana, jalannya algoritma ini dapat digambarkan sebagai dua orang pemain yang masing-masing berperan sebagai Maximizer dan Minimizer. Pemain yang berperan sebagai maximizer akan berusaha 
menghasilkan nilai yang sebesar mungkin, sementara pemain yang berperan sebagai minimizer akan berusaha menghasilkan nilai yang sekecil mungkin. Sejatinya, algoritma ini berjalan berdasarkan prinsip yang berlaku dalam zero-sum game, 
yaitu kenaikan nilai seorang pemain akan menyebabkan penurunan nilai pada pemain lainnya.

Selain itu, untuk memperkecil state space yang akan dicari, akan dilakukan juga teknik Alpha-Beta prunning. Secara umum, teknik ini akan memperkecil state space dengan cara berhenti menjelajah lebih dalam pada suatu Minimax Tree apabila dapat dipastikan bahwa semua pergerakan di dalam subtree bersangkutan tidak akan memberikan pilihan yang lebih baik untuk strategi ke depannya. Dengan demikian, ruang lingkup pencarian akan lebih kecil sehingga waktu jalannya program akan menjadi lebih baik.

Terakhir, akan didefinisikan sebuah fungsi heuristik yang baik yang dapat digunakan untuk meningkatkan performance AI ini. Namun, hal tersebut belum dieksplorasi lebih jauh sehingga tidak dapat didefinisikan saat ini.

* * *

## The Development
Proyek ini akan dikembangkan dan dikemas dalam sebuah *website application* dengan menggunakan framework Django.
* **Python** - *language base*
* **Django framework** - *website application*

## Links
[The Game](http://onitama.lannysport.net/)

[Related Application](https://boardgamegeek.com/thread/1949129/other-games-similar-onitama)

[Game Implementation](https://github.com/TrippW/onitama)