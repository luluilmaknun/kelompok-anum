from django.apps import AppConfig


class OnitamaTemplateConfig(AppConfig):
    name = 'onitama_template'
