
from django.shortcuts import render, redirect
from django.http import HttpResponse

# ini top down indexnya
init_position = {0:'rook2', 1:'rook2', 2:'king2', 3:'rook2', 4:'rook2', 		# black side
				 20:'rook1', 21:'rook1', 22:'king1', 23:'rook1', 24:'rook1'}	# white side

# 'moves' : [(x,y)]
init_card = {
			'card1':{'name':'Rabbit', 'moves':[(-1,-1), (1,1), (-2,0)]}, 
			'card2':{'name':'Crab', 'moves':[(0,1),(2,0),(-2,0)]},		# black card 
			'card3':{'name':'Frog', 'moves':[(-1,1),(1,-1),(2,0)]}, 			# center card
			'card4':{'name':'Eel', 'moves':[(0,-1),(1,-1),(1,1)]},
			'card5':{'name':'Cobra', 'moves':[(-1,-1),(-1,1),(1,0)]}		# white card
			}

is_winning = {1: False,		# black winning
			  2: False}		# white winning

json_data = {'init_position' : init_position, 'init_card' : init_card, 'is_winning' : is_winning}

def show_game(request):
	# make_board()
	return render(request, 'game.html', {'position' : init_position, 'card' : init_card, 'win' : is_winning})

def make_board():
	f = open("onitama_template/templates/board.html", "+w")
	stroke = '#000000'
	fill = ''
	text = "{% load static %}<svg class='board' width='600px' height='800px' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' transform='translate(130,100)'>"
	text+= "<g transform='translate(0,130)'>"
	for i in range(0, 25):
		column = i%5
		row = i//5
		x = column*60
		y = row*60

		if i%2 == 0:
			fill = '#FFFFFF'
		else:
			fill = '#DDDDDD'

		if column == 0:
			text+= "<g>"

		text+= "<rect id='"+str(i)+"' x='"+str(x)+"' y='"+str(y)+"' height='60' width='60' style='stroke-width:3;stroke:"+str(stroke)+"; fill:"+str(fill)+"' />"
		text+= "{% if position."+str(i)+" %}"
		text+= "{% with 'images/'|add:position."+str(i)+" as image %}"
		text+= "<image xlink:href='{% static image|add:'.jpeg' %}' x='"+str(x)+"' y='"+str(y)+"' height='50' width='50' transform='translate(5,5)'/>"
		text+= "{% endwith %}{% endif %}"
		
		if column == 4:
			text+= "</g>"

	text += "</g>"
	text += "{% include 'card.html' %}"
	text += "</svg>"

	f.write(text)
	f.close()

def getJson():
	return