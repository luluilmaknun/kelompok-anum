from django.urls import path
from .views import *

app_name = 'api'
urlpatterns = [
    path('next_state', next_state, name="next_state")
]
