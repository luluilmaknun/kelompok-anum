from django.apps import AppConfig


class OnitamaBackendConfig(AppConfig):
    name = 'onitama_backend'
