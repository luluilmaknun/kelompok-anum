from onitama_backend.game.helper import *


# decide we find max or min, for now we only use max
def decision(game, turn, depth, max=True):
    if max:
        return maximize(game, turn, 1e9, depth)
    else:
        return minimize(game, turn, -1e9, depth)


def maximize(game, turn, min, depth):
    # if we are on the last depth or  some player already win, just compute the utility
    if game.player_win != -1 or depth == 0:
        return [compute_utility(game, turn)]

    utility = -Const.MAX_UTILITY

    # retrieve pawn and card not always player_1
    player_1 = game.get_player(turn)
    player_1_move = player_1.get_move()
    player_1_pawn = player_1.get_pawn()
    player_1_pawn_x = player_1_pawn[0]
    player_1_pawn_y = player_1_pawn[1]

    index_card = []

    for i in range(len(player_1_move)):
        card = player_1_move[i]

        card_move = card.move

        for j in range(len(card_move)):
            step = card_move[j]

            move_x = step[0]
            move_y = step[1]

            for k in range(len(player_1_pawn_x)):
                position_x = player_1_pawn_x[k]
                position_y = player_1_pawn_y[k]

                if position_x == -1 and position_y == -1:
                    continue

                # deep copy of the game
                new_game = game.get_copy()

                player_1 = new_game.get_player(turn)
                player_2 = new_game.get_player(1 - turn)

                # change middle card and the card that is being used
                new_game.middle_card, new_game.list_player[turn].move[
                    i] = new_game.list_player[turn].move[i], new_game.middle_card

                # take the move
                if turn == 0:
                    new_game.list_player[turn].position_x[k] += move_x
                    new_game.list_player[turn].position_y[k] += move_y
                else:
                    new_game.list_player[turn].position_x[k] -= move_x
                    new_game.list_player[turn].position_y[k] -= move_y

                if not check_valid_move(new_game.list_player[turn]):
                    continue

                new_game = evaluate_move(new_game, turn)

                utility_tmp = minimize(
                    new_game, 1 - turn, utility, depth - 1)[0]

                if utility_tmp > min:
                    utility = utility_tmp
                    index_card = [position_x,
                                  position_y, card.description, j]
                    return [utility, index_card]
                # print(utility_tmp)

                if utility_tmp >= utility:
                    utility = utility_tmp
                    index_card = [position_x,
                                  position_y, card.description, j]

    #print('max', depth, utility)
    return [utility, index_card]


def minimize(game, turn, max, depth):
    if game.player_win != -1 or depth == 0:
        return [compute_utility(game, turn)]

    utility = Const.MAX_UTILITY

    player_1 = game.get_player(turn)

    player_1_move = player_1.get_move()
    player_1_pawn = player_1.get_pawn()
    player_1_pawn_x = player_1_pawn[0]
    player_1_pawn_y = player_1_pawn[1]

    index_card = []

    for i in range(len(player_1_move)):
        card = player_1_move[i]

        card_move = card.move

        for j in range(len(card_move)):
            step = card_move[j]

            move_x = step[0]
            move_y = step[1]

            for k in range(len(player_1_pawn_x)):
                position_x = player_1_pawn_x[k]
                position_y = player_1_pawn_y[k]

                if position_x == -1 and position_y == -1:
                    continue

                new_game = game.get_copy()

                player_1 = new_game.get_player(turn)
                player_2 = new_game.get_player(1 - turn)

                new_game.middle_card, new_game.list_player[turn].move[
                    i] = new_game.list_player[turn].move[i], new_game.middle_card

                if turn == 0:
                    new_game.list_player[turn].position_x[k] += move_x
                    new_game.list_player[turn].position_y[k] += move_y
                else:
                    new_game.list_player[turn].position_x[k] -= move_x
                    new_game.list_player[turn].position_y[k] -= move_y

                if not check_valid_move(new_game.list_player[turn]):
                    continue

                new_game = evaluate_move(new_game, turn)

                utility_tmp = maximize(
                    new_game, 1 - turn, utility, depth - 1)[0]

                if utility_tmp < max:
                    utility = utility_tmp
                    index_card = [position_x,
                                  position_y, card.description, j]
                    return [utility, index_card]

                if utility_tmp <= utility:
                    utility = utility_tmp
                    index_card = [position_x,
                                  position_y, card.description, j]

    #print('min', depth, utility)
    return [utility, index_card]
