from onitama_backend.game import Const


class Player:
    DEST_NOT_EMPTY = -1
    SRC_EMPTY = -2
    SUCCESS_MOVE = 1

    def __init__(self, move, position_x, position_y, position_win):
        """
        move is list of card player have in hand
        position win is the opponent's king initial position
        """
        self.move = move
        self.position = list(zip(position_x, position_y))
        self.position_x = position_x
        self.position_y = position_y
        self.position_win = position_win
        self.enemy = self

    def move_pawn(self, coor_bf, coor_af):
        print(self.position)
        """
        :param coor_bf: coordinate pawn before moved
        :param coor_af: coodinate pawn after moved
        :return: void
        """
        if coor_af in self.position:
            print(Const.DEST_NOT_EMPTY_WARNING)
            return self.DEST_NOT_EMPTY

        if coor_bf not in self.position:
            print(Const.SRC_EMPTY_WARNING)
            return self.SRC_EMPTY

        idx = self.position.index(coor_bf)
        self.position[idx] = coor_af

        print(self.position_x, idx, coor_af, self.position)
        self.position_x[idx] = coor_af[0]
        self.position_y[idx] = coor_af[1]

        if coor_af in self.enemy.position:
            idx = self.enemy.position.index(coor_af)
            self.enemy.position[idx] = (-1, -1)

            print(Const.ENEMY_PAWN_EATEN_WARNING)

        return self.SUCCESS_MOVE

    def set_enemy(self, other_player):
        self.enemy = other_player

    def has_card_name(self, name):
        for card in self.move:
            if name == card.description:
                return True

        return False

    def has_pawn_coor(self, coor):
        return coor in self.position

    def get_card_by_name(self, name):
        for card in self.move:
            if name == card.description:
                return card

        return False

    def get_move(self):
        return self.move

    def get_pawn(self):
        return self.position_x, self.position_y

    def get_position_x(self):
        position_x = []
        for i in range(len(self.position)):
            position_x.append(self.position[i][0])

        return position_x

    def get_position_y(self):
        position_y = []
        for i in range(len(self.position)):
            position_y.append(self.position[i][1])

        return position_y
