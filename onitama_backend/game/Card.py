"""
Class to create cards based on the list in Const.py
"""


class Card:

    def __init__(self, description, move_x, move_y):
        """
        description is the card name
        move is a list of tuples of possible moves
        """
        self.description = description
        self.move = []

        for i in range(len(move_x)):
            self.move.append((move_x[i], move_y[i]))

    def __str__(self):
        return "Card Type: " + str(self.description) + "\nmove : " + str(self.move)
