import copy
import random

from onitama_backend.game.Card import *
from onitama_backend.game.GameSimulator import *


class Game:
    """
    board is 5x5, lowest left is 1, 1, while upper right is 5, 5
    white is at the lower left by default
    x is the y axis in cartesian, which is 2, 1 is upper of 1, 1
    """
    PION_PUTIH = "P"
    PION_HITAM = "p"
    RAJA_PUTIH = "K"
    RAJA_HITAM = "k"
    MAX_DEPTH = 1

    def __init__(self, state={}):
        """
        Initialization of Onitama Game, from choose the 5 random cards from the list and give it to each player
        """
        # Take the last 5 cards in the the randomed list for the game
        print(len(state))
        if len(state) == 0:
            list_move = self.precompute_card()
            random.shuffle(list_move)
            game_move = list_move[:5]
        else:
            game_move = state["game_move"]
            game_move = self.create_cards(game_move)

        for i in game_move:
            print(i.description, " card is in the game")

        self.middle_card = game_move[-1]
        self.list_player = []
        self.player_turn = 0

        # The initialization of Players' initial places
        # the last value in the second and third parameter is the king's position
        if len(state) == 0:
            player_x = [1, 1, 1, 1, 1]
            player_y = [1, 2, 4, 5, 3]
        else:
            player_x = state["player_0_row"]
            player_y = state["player_0_col"]

        player_win_coor = [5, 3]

        player = Player(game_move[:2], player_x, player_y, player_win_coor)
        self.list_player.append(player)

        if len(state) == 0:
            player_x = [5, 5, 5, 5, 5]
            player_y = [1, 2, 4, 5, 3]
        else:
            player_x = state["player_1_row"]
            player_y = state["player_1_col"]

        player_win_coor = [1, 3]

        player = Player(game_move[2:-1], player_x, player_y, player_win_coor)
        self.list_player.append(player)

        self.list_player[0].set_enemy(self.list_player[1])
        self.list_player[1].set_enemy(self.list_player[0])

        self.player_win = -1

    def precompute_card(self):
        """
        Method to generate cards from the Const.py
        return the card list
        """
        list_move = []

        for card in Const.LIST_CARD:
            description = card[0]
            move_x = card[1]
            move_y = card[2]

            card = Card(description, move_x, move_y)
            list_move.append(card)

        return list_move

    def create_cards(self, game_move):
        """
        Method to create card objects if given the cards name
        :param game_move: cards name
        :return: cards object
        """
        cards = []
        for card in Const.LIST_CARD:
            if card[0].lower() in game_move:
                description = card[0]
                move_x = card[1]
                move_y = card[2]

                card = Card(description, move_x, move_y)
                cards.append(card)

        assert(len(cards) == 5)
        for i in range(len(cards)):
            for j in range(i + 1, len(cards)):
                if cards[j].description.lower() == game_move[i].lower():
                    cards[i], cards[j] = cards[j], cards[i]

        return cards

    def print_board(self):
        """
        Method to print the board
        -------------------------
        p stands for black pawns, k stands for black king, P white pawns, K white king
        cur_playing is the currently playing player
        """
        player_0 = self.list_player[0]
        player_1 = self.list_player[1]
        ind_playing = self.player_turn

        print()
        print(Const.LIMITER_2)
        print("Player", ind_playing + 1, "Turn")
        self.print_card()

        start_range = 1
        finish_range = 6
        delta = 1

        if ind_playing + 1 == 2:
            start_range = 5
            finish_range = 0
            delta = -1

        print(Const.LIMITER_1)
        print("BOARD:")
        print(Const.LIMITER_1)
        for i in range(start_range, finish_range, delta):
            for j in range(start_range, finish_range, delta):
                ii = 6 - i
                jj = j

                if player_0.position[-1] == (ii, jj):
                    print(self.RAJA_PUTIH, end='')
                elif player_1.position[-1] == (ii, jj):
                    print(self.RAJA_HITAM, end='')
                elif (ii, jj) in player_0.position:
                    print(self.PION_PUTIH, end='')
                elif (ii, jj) in player_1.position:
                    print(self.PION_HITAM, end='')
                else:
                    print(".", end='')
            print()
        print(Const.LIMITER_1)

    def print_card(self):
        """
        Method to print cards
        :return: void
        """
        ind_playing = self.player_turn
        print(Const.LIMITER_1)
        print("CARD:")
        print(Const.LIMITER_1)
        for i in range(2):
            ind_loop = (ind_playing + i) % 2
            player_loop = self.list_player[ind_loop]
            print(">> Player {}".format(ind_loop + 1))

            for card in player_loop.move:
                print("   {} ({}) :".format(
                    card.description, len(card.move)), end='')

                for move in card.move:
                    print(" {},".format(move), end='')

                print()

        print(">> Middle card")
        print("   {} ({}) :".format(self.middle_card.description,
                                    len(self.middle_card.move)), end='')

        for move in self.middle_card.move:
            print(" {},".format(move), end='')

        print()

    def change_turn(self):
        self.player_turn = 1 - self.player_turn

    def get_cur_player(self):
        return self.list_player[self.player_turn]

    def convert_by_player(self, coor):
        """
        Convert given coordinate based on point of view in CLI to coordinate in the program, which
        is not relative to point of view
        :param coor: coordinate to be converted
        :return: coordinate after converted
        """
        if self.player_turn == 1:
            coor = (6 - coor[0], 6 - coor[1])

        return tuple(coor)

    def next_coor(self, coor_bf, delta):
        """
        Count next coordinate if current pawn moved by delta based on point of view of current player
        :param coor_bf: coordinate of the pawn before moving
        :param delta: how long the step that the pawn going to take, based on point of view of current player
        :return:
        """
        if self.player_turn == 0:
            return coor_bf[0] + delta[0], coor_bf[1] + delta[1]
        else:
            return coor_bf[0] - delta[0], coor_bf[1] - delta[1]

    def swap_card(self, card_str):
        """
        Method to swap card whose name is card_str to middle_card
        :param card_str: current player card that will be swapped
        :return: void
        """
        cur_player = self.list_player[self.player_turn]
        idx = -1
        for i in range(len(cur_player.move)):
            if cur_player.move[i].description == card_str:
                idx = i
                break
        cur_player.move[idx], self.middle_card = self.middle_card, cur_player.move[idx]

    def is_winning(self):
        """
        Check if current player win in this current state or not
        :return: True if win, False otherwise
        """
        cur_player = self.list_player[self.player_turn]
        opp_player = self.list_player[1 - self.player_turn]

        if opp_player.position[-1] == (-1, -1):
            return True

        for pos in cur_player.position:
            if pos == cur_player.position_win:
                return True

        return False

    def print_win(self):
        print("Player " + (self.player_turn + 1).__str__() + " win the game!")

    def get_player(self, turn):
        return self.list_player[turn]

    def get_copy(self):
        return copy.deepcopy(self)

    def get_game_move_str(self):
        list_card_player_0 = self.list_player[0].get_move()
        list_card_player_1 = self.list_player[1].get_move()
        middle_card = self.middle_card
        moves_str = []
        for card in list_card_player_0:
            moves_str.append(card.description)
        for card in list_card_player_1:
            moves_str.append(card.description)
        moves_str.append(middle_card.description)

        return moves_str