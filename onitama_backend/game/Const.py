"""
Strings that be used as constants in the program
"""
LIMITER_1 = "----------------------"
LIMITER_2 = "======================"
DEST_NOT_EMPTY_WARNING = "There is your pawn in your destination coordinate"
SRC_EMPTY_WARNING = "No pawn in your source coordinate"
ENEMY_PAWN_EATEN_WARNING = "Enemy pawn has been eaten"
PLAYER_NOT_HAS_CARD_NAME = "Your input card doesn't exist in your card"
MOVE_INDEX_DOES_NOT_EXIST = "Move index is invalid"
INVALID_COORDINATE = "Invalid coordinate"
NO_PLAYER_CARD_IN_GIVEN_COORDINATE = "Your card doesn't exist in your given coordinate"
INDEX_INVALID = "Index invalid"
PAWN_MOVE_OUT_OF_BOARD = "Invalid move, pawn move out of board"
TABLE_POV = "Row 1 - 5 : bottom - up\n" +\
			"Col 1 - 5 : left - right"
GAME_OVER = "+++++++GAME OVER+++++++"

"""
List of tuples that will be used in the game
Tuple format: ("card name", [list of x spaces moves], [list of y spaces moves])
"""
LIST_CARD = [
    (
        "Tiger",
        [2, -1],
        [0, 0],
    ),
    (
        "Dragon",
        [1, 1, -1, -1],
        [-2, 2, -1, 1],
    ),
    (
        "Frog",
        [1, 0, -1],
        [-1, -2, 1],
    ),
    (
        "Rabbit",
        [1, 0, -1],
        [1, 2, -1],
    ),
    (
        "Crab",
        [1, 0, 0],
        [0, -2, 2],
    ),
    (
        "Elephant",
        [1, 1, 0, 1],
        [-1, 1, 1, 0],
    ),
    (
        "Goose",
        [1, 0, 0, -1],
        [-1, -1, 1, 1],
    ),
    (
        "Rooster",
        [1, 0, 0, -1],
        [1, 1, -1, -1],
    ),
    (
        "Monkey",
        [1, 1, -1, -1],
        [-1, 1, -1, 1],
    ),
    (
        "Mantis",
        [1, 1, -1],
        [-1, 1, 0],
    ),
    (
        "Horse",
        [1, 0, -1],
        [0, -1, 0],
    ),
    (
        "Ox",
        [1, 0, -1],
        [0, 1, 0],
    ),
    (
        "Crane",
        [1, -1, -1],
        [0, -1, 1],
    ),
    (
        "Boar",
        [1, 0, 0],
        [0, -1, 1],
    ),
    (
        "Eel",
        [1, 0, -1],
        [-1, 1, -1],
    ),
    (
        "Cobra",
        [1, 0, -1],
        [1, -1, 1],
    ),
]

MAX_UTILITY = 1000000000
