from onitama_backend.game.Game import *
from onitama_backend.game.GameSimulator import *


def main():
    """
    main function of the game
    :return:
    """
    game = Game()
    simulator = GameSimulator(game)
    simulator.play()


if __name__ == "__main__":
    main()
