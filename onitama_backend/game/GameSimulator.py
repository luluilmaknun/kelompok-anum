from onitama_backend.game.minmax import *
from onitama_backend.game.Player import *


class GameSimulator:
    """
    Simulator of the game
    """

    def __init__(self, _game):
        """
        Initialization of the simulator
        :param _game: Game that be used
        """
        self.game = _game

    def player_turn(self, pawn_coor, card_str, move_idx):
        cur_player = self.game.get_cur_player()
        try:
            print(pawn_coor)

            pawn_coor = self.game.convert_by_player(pawn_coor)
            if not cur_player.has_pawn_coor(pawn_coor):
                print(Const.NO_PLAYER_CARD_IN_GIVEN_COORDINATE)
                return -1
        except:
            print(Const.INVALID_COORDINATE)
            return -1

        print(card_str)

        if not cur_player.has_card_name(card_str):
            print(Const.PLAYER_NOT_HAS_CARD_NAME)
            return -1

        card = cur_player.get_card_by_name(card_str)
        try:
            print(move_idx)

            move_idx -= 1
            if move_idx >= len(card.move) or move_idx < 0:
                print(Const.MOVE_INDEX_DOES_NOT_EXIST)
                return -1
        except:
            print(Const.INDEX_INVALID)
            return -1

        delta = card.move[move_idx]
        coor_pawn_bf = pawn_coor
        coor_pawn_af = self.game.next_coor(coor_pawn_bf, delta)

        print(delta, coor_pawn_bf, coor_pawn_af)
        if not in_constraint(coor_pawn_af, 1, 5, 1, 5):
            print(Const.PAWN_MOVE_OUT_OF_BOARD)
            return -1

        if cur_player.move_pawn(coor_pawn_bf, coor_pawn_af) == Player.SUCCESS_MOVE:
            self.game.swap_card(card_str)

            if self.game.is_winning():
                self.game.print_win()
                return 2

            self.game.change_turn()
            return 1

        return -1

    def ai_turn(self):
        cur_player = self.game.get_cur_player()
        result = decision(self.game, self.game.player_turn, self.game.MAX_DEPTH)

        tmp = result[1]

        print(result)
        card_str = tmp[2]
        card = cur_player.get_card_by_name(card_str)
        delta = card.move[tmp[3]]
        coor_pawn_bf = tuple((tmp[0], tmp[1]))

        print(coor_pawn_bf, card, delta)
        coor_pawn_af = self.game.next_coor(coor_pawn_bf, delta)

        if cur_player.move_pawn(coor_pawn_bf, coor_pawn_af) == Player.SUCCESS_MOVE:
            self.game.swap_card(card_str)

            if self.game.is_winning():
                self.game.print_win()
                return 2

            self.game.change_turn()
            return 1
        return -1

    def play(self):
        """
        Simulating play the game
        :return: void
        """
        print(Const.LIMITER_1)
        print(Const.TABLE_POV)
        while True:
            self.game.print_board()

            cur_player = self.game.get_cur_player()

            if self.game.player_turn == 1:
                result = decision(self.game, self.game.player_turn, self.game.MAX_DEPTH)

                tmp = result[1]

                print(result)
                card_str = tmp[2]
                card = cur_player.get_card_by_name(card_str)
                delta = card.move[tmp[3]]
                coor_pawn_bf = tuple((tmp[0], tmp[1]))

                print(coor_pawn_bf, card, delta)
                coor_pawn_af = self.game.next_coor(coor_pawn_bf, delta)

                if cur_player.move_pawn(coor_pawn_bf, coor_pawn_af) == Player.SUCCESS_MOVE:
                    self.game.swap_card(card_str)

                    if self.game.is_winning():
                        self.game.print_win()
                        break

                    self.game.change_turn()

            else:
                while True:
                    pawn_coor = input(
                        "Choose pawn position to move \"(row, col)\": ")
                    try:
                        pawn_coor = eval(pawn_coor)

                        print(pawn_coor)

                        pawn_coor = self.game.convert_by_player(pawn_coor)
                        if not cur_player.has_pawn_coor(pawn_coor):
                            print(Const.NO_PLAYER_CARD_IN_GIVEN_COORDINATE)
                            continue
                        break
                    except:
                        print(Const.INVALID_COORDINATE)
                        continue

                while True:
                    card_str = input("Choose card: ")

                    print(card_str)

                    if not cur_player.has_card_name(card_str):
                        print(Const.PLAYER_NOT_HAS_CARD_NAME)
                        continue
                    break

                move_idx = -1
                while True:
                    card = cur_player.get_card_by_name(card_str)
                    try:
                        move_idx = int(
                            input("Choose move index from the card (start with 1): "))

                        print(move_idx)

                        move_idx -= 1
                        if move_idx >= len(card.move) or move_idx < 0:
                            print(Const.MOVE_INDEX_DOES_NOT_EXIST)
                            continue
                        break
                    except:
                        print(Const.INDEX_INVALID)
                        continue

                delta = card.move[move_idx]
                coor_pawn_bf = pawn_coor
                coor_pawn_af = self.game.next_coor(coor_pawn_bf, delta)

                print(delta, coor_pawn_bf, coor_pawn_af)
                if not in_constraint(coor_pawn_af, 1, 5, 1, 5):
                    print(Const.PAWN_MOVE_OUT_OF_BOARD)
                    continue

                if cur_player.move_pawn(coor_pawn_bf, coor_pawn_af) == Player.SUCCESS_MOVE:
                    self.game.swap_card(card_str)

                    if self.game.is_winning():
                        self.game.print_win()
                        break

                    self.game.change_turn()

        print(Const.GAME_OVER)


def in_constraint(coor, low_r, high_r, low_c, high_c):
    """
    Check if given coordinate satisfied the constraint or not
    :param coor: coordinate that will be checked
    :param low_r: lower bound (inclusive) of coordinate row
    :param high_r: upper bound (inclusive) of coordinate row
    :param low_c: lower bound (inclusive) of coordinate column
    :param high_c: upper bound (inclusive) of coordinate column
    :return: True if the coordinate satisfy the constraint, False otherwise
    """
    return low_r <= coor[0] <= high_r and low_c <= coor[1] <= high_c
