from onitama_backend.game import Const


def check_valid_move(player):

    player_pawn = player.get_pawn()
    player_pos_x = player_pawn[0]
    player_pos_y = player_pawn[1]

    for i in range(len(player_pos_x)):
        pos_x = player_pos_x[i]
        pos_y = player_pos_y[i]

        if pos_x == -1 and pos_y == -1:
            continue

        # check if there is
        if (pos_x != -1 and pos_y != -1) and (pos_x < 1 or pos_x > 5 or pos_y < 1 or pos_y > 5):
            return False

        # check if there is 2 pawn in the same place
        for j in range(i):
            pos_x_tmp = player_pos_x[j]
            pos_y_tmp = player_pos_y[j]

            if pos_x == pos_x_tmp and pos_y == pos_y_tmp and pos_x != -1 and pos_y != -1:
                return False

    return True


def evaluate_move(game, turn):
    player_now = game.list_player[turn]
    player_enemy = game.list_player[1 - turn]

    player_now_pawn = player_now.get_pawn()
    player_now_x = player_now_pawn[0]
    player_now_y = player_now_pawn[1]

    player_enemy_pawn = player_enemy.get_pawn()
    player_enemy_x = player_enemy_pawn[0]
    player_enemy_y = player_enemy_pawn[1]

    for i in range(len(player_now_x)):
        pos_now_x = player_now_x[i]
        pos_now_y = player_now_y[i]

        if pos_now_x == -1 and pos_now_y == -1:
            continue

        for j in range(len(player_enemy_x)):
            pos_enemy_x = player_enemy_x[j]
            pos_enemy_y = player_enemy_y[j]

            if pos_now_x == pos_enemy_x and pos_now_y == pos_enemy_y:
                if j == len(player_enemy_x) - 1:
                    game.player_win = turn

                player_enemy_x[j] = -1
                player_enemy_y[j] = -1

                return game

    if player_now_x[-1] == player_now.position_win[0] and player_now_y[-1] == player_now.position_win[1]:
        game.player_win = turn
        return game

    return game


def compute_utility(game, turn):
    if game.player_win == 1:
        game.player_win = -1
        return Const.MAX_UTILITY
    elif game.player_win == 0:
        game.player_win = -1
        return -Const.MAX_UTILITY

    count_1 = 0
    count_2 = 0
    for i in game.list_player[1].position_x:
        if i != -1:
            count_1 += 1

    for i in game.list_player[0].position_x:
        if i != -1:
            count_2 += 1
    return count_1 - count_2
