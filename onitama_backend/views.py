from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import json
from onitama_backend.game.Game import *


@csrf_exempt
def next_state(request):
    if request.method == 'POST':
        game_move = request.POST["game_move"]
        game_move = json.loads(game_move)
        player_0_row = request.POST["player_0_row"]
        player_0_row = json.loads(player_0_row)
        player_0_col = request.POST["player_0_col"]
        player_0_col = json.loads(player_0_col)
        player_1_row = request.POST["player_1_row"]
        player_1_row = json.loads(player_1_row)
        player_1_col = request.POST["player_1_col"]
        player_1_col = json.loads(player_1_col)

        state = {
            "game_move": game_move,
            "player_0_row": player_0_row,
            "player_0_col": player_0_col,
            "player_1_row": player_1_row,
            "player_1_col": player_1_col
        }
        game = Game(state)
        simulator = GameSimulator(game)
        game.print_board()

        if game.is_winning():
            state["is_winning"] = True
            state["winner"] = game.player_turn
        else:
            game.change_turn()
            resp = simulator.ai_turn()
            if game.is_winning():
                state["is_winning"] = True
                state["winner"] = game.player_turn
            else:
                state["is_winning"] = False
                state["winner"] = -1

        state["player_0_row"] = game.get_player(0).position_x
        state["player_0_col"] = game.get_player(0).position_y
        state["player_1_row"] = game.get_player(1).position_x
        state["player_1_col"] = game.get_player(1).position_y
        state["game_move"] = game.get_game_move_str()
        return JsonResponse(state)

    return JsonResponse({'status': 400})
